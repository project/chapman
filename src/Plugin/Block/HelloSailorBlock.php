<?php

namespace Drupal\chapman\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a "I don't like spam!" block.
 *
 * @Block(
 *   id = "hello_sailor_block",
 *   admin_label = @Translation("Hello sailor block"),
 *   category = @Translation("Hello sailor"),
 * )
 */
class HelloSailorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $element = array(
        '#markup' => '<p>' . t("I don't like spam!") . '</p>',
    );
    return $element;
  }

}
