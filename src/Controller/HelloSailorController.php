<?php

namespace Drupal\chapman\Controller;

/**
 * Provides route responses for the Guided Custom module.
 */
class HelloSailorController {

    /**
     * Returns a simple page.
     *
     * @return array
     *   A simple renderable array.
     */
    public function sayhello() {
        $element = array(
            '#markup' => '<p>' . t("I don't like spam!") . '</p>',
        );
        return $element;
    }

}
