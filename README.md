# Contents of this file

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


## INTRODUCTION

So far this module is just a stub.

The plan is to create an antispam module that combines a number of
heuristics to compute a spam score 0-100 (where 0 is ham, and 100 is
100 % spam).

### Proposed heuristics

1. Language.  Detect language, if not on allowed list: spam.
2. Links. Count links. More links: spam.
3. Words: If on disallowed list (e.g. viagra, sildenafil, porn): spam
4. Forbidden domains: record domains used by spammers. If fit: spam,
5. Author/role: anoynmous increases score. confirmed zero's score.

Provide it as a web service (SaaS).  Client sends message to be
analyzed to server, as well as some other paraamters such as author.

Returns a number between 0-100.

Client also let user set:

- Unpublish lower limit (e.g. 50 unpublishes everthing with a score >= 50).
- Days to retain unpublished before automatically purging.

Admins can inspect queues:

- Unpublished - to look for false positives.
- Published by anonymous/unconfirmed - to look for false positives.


## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

* Install as you would normally install a contributed Drupal
  module. Visit [Installing modules][1] for further information.


## CONFIGURATION

[TBA]

## MAINTAINERS

The current maintainer is [gisle][2] (Gisle Hannemyr).  
This project has been sponsored by [Hannemyr Nye Medier AS][3].

Any help with development (patches, reviews, comments) are welcome.

[1]  https://www.drupal.org/node/1897420
[2]: https://www.drupal.org/u/gisle
[3]: https://hannemyr.no
